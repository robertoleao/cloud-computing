#-------------------------------------Zona de disponibilidade A ----------------------------------------#
resource "aws_subnet" "frontend-01" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.1.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = true
tags = {
   Name = "frontend-01"
  }
}
resource "aws_subnet" "backend-01" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.2.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false
tags = {
   Name = "backend-01"
  }
}
resource "aws_subnet" "db-01" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.3.0/24"
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false
tags = {
   Name = "db-01"
  }
}
#-------------------------------------Zona de disponibilidade B ----------------------------------------#
resource "aws_subnet" "frontend-02" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.4.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = true
tags = {
   Name = "frontend-02"
  }
}
resource "aws_subnet" "backend-02" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.5.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = false
tags = {
   Name = "backend-02"
  }
}
resource "aws_subnet" "db-02" {
  vpc_id                  = aws_vpc.My_VPC.id
  cidr_block              = "10.1.6.0/24"
  availability_zone = "us-east-1b"
  map_public_ip_on_launch = false
tags = {
   Name = "db-02"
  }
}