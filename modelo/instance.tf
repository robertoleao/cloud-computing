#-------------------------------------Zona de disponibilidade A ----------------------------------------#

resource "aws_instance" "webserver-01" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.frontend-01.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "webserver-01"
    }
  }

  resource "aws_instance" "backend-01" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.backend-01.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "backend-01"
    }
  }

  resource "aws_instance" "db-01" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.db-01.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "db-01"
    }
  }

  #-------------------------------------Zona de disponibilidade B ----------------------------------------#

  resource "aws_instance" "webserver-02" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.frontend-02.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "webserver-02"
    }
  }

  resource "aws_instance" "backend-02" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.backend-02.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "backend-02"
    }
  }

  resource "aws_instance" "db-02" {
  ami           = var.ec2image
  instance_type = "t2.medium"
  subnet_id     = aws_subnet.db-02.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name = var.ssh_key_name
  tags          = {
    Name = "db-02"
    }
  }