output "webserver-01" {
 value =  aws_instance.webserver-01.public_ip
}

output "webserver-02" {
 value =  aws_instance.webserver-02.public_ip
}