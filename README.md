# Cloud Computing

![Capa da materia](https://gitlab.com/robertoleao/cloud-computing/-/raw/master/image/logo-aws.png)

Aula na AWS com Pablo Marques Menezes com suporte da AWS Educate, Aws Academy Instructure utilizando o modulo AWS Academy Cloud Foundation (Fundamentos de nuvem do AWS Academy) 

É direcionado para estudantes que procuram um entendimento geral dos conceitos de computação em nuvem, independentemente de funções técnicas específicas. O curso provê uma visão geral detalhada dos conceitos de nuvem, principais serviços AWS, segurança, arquitetura, definição de preço e suporte
